## Instalation
```sh
$ git clone https://Durasel@bitbucket.org/Durasel/esm-landing.git
$ npm install
$ bower install
```
---

## Start

```sh
$ gulp - start development
$ node tournaments.js - start webscocket server
```

---

## API

**Websockets messages:**



**Add tournament**

```sh
JSON.stringify({ event: "add_tournament", data: tournament })
```

Where **tournament** json object

```sh
{
	id: "1", // tournament id
	game_style: "dota", // game style for game background color (dota, artifact, lol)
	game_name: "Dota2", // game name
	image: "/img/dotat.png", // tournament image
	name: "Tournament name", // tournament name
	start_time: 000000000, // epoch time of tournament start
	end_time: 000000000, // epoch time of tournament end
	additional_info: ["5 vs 5", "1000$*"] // additional information about tournament
}
```



**Delete tournament**

```sh
JSON.stringify({ event: "delete_tournament", data: "1" })
```

Where **data** string of tournament id



**Change badge**

```sh
JSON.stringify({ event: "change_tournament_badge", data: badge})
```

Where **badge** json object

```sh
{
	id: "5", // tournament id
	badge: '<span class="badge badge-danger">Турнир окончен</span>' // new badge html
}
```

## Other

Tournaments percentages checks every 30 secs in main.js:122







