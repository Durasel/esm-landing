"use strict";

function addTournament(data) {

    var today = new Date();
    var tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
    var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    var start_time = new Date(data.start_time);
    var end_time = new Date(data.end_time);

    var wrap = document.createElement('div');
    var tournament_html =
        '<a href="#!" class="tournament-card add ' + data.game_style + '" id="' + data.id + '">' +
        '<div class="game-name">' +
        '<span>' + data.game_name + '</span>' +
        '</div>' +
        '<div class="tournament-image" style="background-image: url(' + data.image + ');">' +
        '</div>' +
        '<div class="main-info">' +
        '<h3>' + data.name + '</h3>';

    // Сегодня
    if (today.getFullYear() == start_time.getFullYear() && today.getMonth() == start_time.getMonth() && today.getDate() == start_time.getDate()) {
        tournament_html += '<span class="day">Сегодня</span>';
    } else if (tomorrow.getFullYear() == start_time.getFullYear() && tomorrow.getMonth() == start_time.getMonth() && tomorrow.getDate() == start_time.getDate()) {
        tournament_html += '<span class="day">Завтра</span>';
    } else if (yesterday.getFullYear() == start_time.getFullYear() && yesterday.getMonth() == start_time.getMonth() && yesterday.getDate() == start_time.getDate()) {
        tournament_html += '<span class="day">Вчера</span>';
    }

    // Date
    tournament_html += '<time datetime="' + new Date(start_time).toLocaleString() + '">' + ('0' + start_time.getDate()).slice(-2) + '.' + ('0' + (start_time.getMonth() + 1)).slice(-2) + ' ' + ('0' + start_time.getHours()).slice(-2) + ':' + ('0' + start_time.getMinutes()).slice(-2) + '</time>' +
        '<br>';

    // Badge
    if (today.getTime() >= end_time.getTime()) {
        tournament_html += '<span class="badge badge-danger">Турнир окончен</span>';
    } else if (today.getTime() >= start_time.getTime()) {
        tournament_html += '<span class="badge badge-success">Турнир уже идет - ' + tournamentPercentage(start_time, today, end_time) + '</span>';
        changeTournamentPercentage(start_time, end_time, data.id)
    } else if (today.getTime() < start_time.getTime()) {
        tournament_html += '<span class="badge badge-warning">Регистрация открыта</span>'
    }

    tournament_html +=
        '</div>';

    if (data.additional_info) {
        tournament_html += '<div class="additional-info">';
        for (let info of data.additional_info) {
            tournament_html += '<div><h3>' + info + '</h3></div>'

        }
        tournament_html += '</div>';
    }

    tournament_html +=
        '<span class="more"></span>' +
        '</a>';

    wrap.innerHTML = tournament_html
    var tournament_card = wrap.firstChild;
    var first_card = document.querySelector('.tournament-card');
    var ghost_element = document.createElement("div");
    ghost_element.classList.add('ghost-tournament-card');
    ghost_element.style.height = '0px';
    first_card.parentNode.insertBefore(ghost_element, first_card)
    setTimeout(function() {
        ghost_element.style.height = first_card.offsetHeight + 'px';
        setTimeout(function() {
            document.querySelector('#tournaments').prepend(tournament_card)
            ghost_element.remove()
            setTimeout(function() {
                tournament_card.classList.remove('add')
            }, 30);
        }, 350);
    }, 30);

}

function deleteTournament(id) {
    var dom_tournament = document.getElementById(id)
    dom_tournament.classList.add('remove')

    setTimeout(function() {
        var ghost_element = document.createElement("div")
        ghost_element.classList.add('ghost-tournament-card')
        ghost_element.style.height = dom_tournament.offsetHeight + 'px';
        dom_tournament.parentNode.insertBefore(ghost_element, dom_tournament)
        setTimeout(function() {
            ghost_element.style.height = '0px';
            setTimeout(function() {
                ghost_element.remove()
            }, 350);
        }, 30);
        dom_tournament.remove()

    }, 500);
}

function changeTournamentBadget(id, badge) {
    if (document.getElementById(id)) {
        document.getElementById(id).querySelector('.badge').remove();
        document.getElementById(id).querySelector('.main-info').insertAdjacentHTML('beforeend', badge)
    }
}

function tournamentPercentage(start_time, today, end_time) {
    return ('<span class="tournament-percentage">' + (((today - start_time) * 100) / (end_time - start_time)).toFixed(0) + '%</span>')
}

function changeTournamentPercentage(start_time, end_time, id) {

    setInterval(function() {
        if (document.getElementById(id) && document.getElementById(id).querySelector('.tournament-percentage')) {
            let today = new Date();
            let percentage_element = document.getElementById(id).querySelector('.tournament-percentage')
            let percentage_string = (((today - start_time) * 100) / (end_time - start_time)).toFixed(0) + '%';

            if (percentage_string !== percentage_element.innerText) {
                percentage_element.innerText = percentage_string;
            }
        }
    }, 30000);
}


document.addEventListener("DOMContentLoaded", function() {
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    var connection = new WebSocket('ws://127.0.0.1:1337');

    connection.onopen = function() {
        // connection is opened and ready to use
    };

    connection.onerror = function(error) {
        // an error occurred when sending/receiving data
    };

    connection.onmessage = function(message) {
        // try to decode json (I assume that each message
        // from server is json)
        try {
            var json = JSON.parse(message.data);

            if (json.event == "add_tournament") {
                addTournament(json.data)
            } else if (json.event == "delete_tournament") {
                deleteTournament(json.data)
            } else if (json.event == "change_tournament_badge") {
                changeTournamentBadget(json.data.id, json.data.badge)
            }

        } catch (e) {
            console.log(e)
            return;
        }
        // handle incoming message
    };

    document.querySelector('[data-toggle="collapse"]').onclick = function() {
        if(this.getAttribute("aria-expanded") == "false") {
            this.setAttribute("aria-expanded", "true");
            document.querySelector(this.getAttribute("data-target")).classList.add('show')
        } else {
            this.setAttribute("aria-expanded", "false");
            document.querySelector(this.getAttribute("data-target")).classList.remove('show')
        }
        
    }

})
//# sourceMappingURL=main.js.map
