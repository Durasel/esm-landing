var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets
    // server we don't have to implement anything.
});
server.listen(1337, function() {});

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});



const tournament_1 = {
    id: "4",
    game_style: "dota",
    game_name: "Dota 2",
    image: "https://cdn-images-1.medium.com/max/800/1*iNCMcjK1jyXdjQu2B_ip6A.jpeg",
    name: "The International 2017",
    start_time: new Date().getTime() - 86400000*365 - 3600000*3,
    end_time: new Date().getTime() - 86400000*365 - 3600000*1,
    additional_info: ["5 vs 5", "1000000$*"]

}

const tournament_2 = {
    id: "4",
    game_style: "dota",
    game_name: "Dota 2",
    image: "https://u.kanobu.ru/editor/images/21/bad81c85-4b90-4446-b217-034c1d64c04a.jpg",
    name: "The International 2018",
    start_time: new Date().getTime() - 86400000 - 3600000*3,
    end_time: new Date().getTime() - 86400000 - 3600000*1,
    additional_info: ["5 vs 5", "1000000$*"]

}

const tournament_3 = {
    id: "5",
    game_style: "artifact",
    game_name: "Artifact",
    image: "https://steamcdn-a.akamaihd.net/steam/apps/583950/header.jpg?t=1545857317",
    name: "Artifact cup 2019",
    start_time: new Date().getTime() + 86400000,
    end_time: new Date().getTime() + 86400000 + 3600000*3,
    additional_info: ["1 vs 1"]
}

const tournament_4 = {
    id: "6",
    game_style: "default",
    game_name: "Радуга",
    image: "https://i.playground.ru/i/blog/326288/content/t1oah2bc.jpg",
    name: "ESM кириллица тест",
    start_time: new Date().getTime() - 3600000,
    end_time: new Date().getTime() + 3600000*3,
    additional_info: ["Заложники", "5 vs 5", "1000$*"]
}

// WebSocket server
wsServer.on('request', function(request) {
    var connection = request.accept(null, request.origin);

    console.log('connected')
    
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "add_tournament", data: tournament_1 }))
    }, 1000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "add_tournament", data: tournament_2 }))
    }, 3000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "add_tournament", data: tournament_3 }))
    }, 5000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "add_tournament", data: tournament_4 }))
    }, 7000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "change_tournament_badge", data: {id: "5", badge: '<span class="badge badge-success">Турнир уже идет - <span class="tournament-percentage">0%</span></span>'} }))
    }, 9000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "change_tournament_badge", data: {id: "5", badge: '<span class="badge badge-success">Турнир уже идет - <span class="tournament-percentage">100%</span></span>'} }))
    }, 10000);
    setTimeout(function() {
        connection.send(JSON.stringify({ event: "change_tournament_badge", data: {id: "5", badge: '<span class="badge badge-danger">Турнир окончен</span>'} }))
    }, 11000);

    

    setTimeout(function() {
        connection.send(JSON.stringify({ event: "delete_tournament", data: "1" }))
        connection.send(JSON.stringify({ event: "delete_tournament", data: "2" }))
        connection.send(JSON.stringify({ event: "delete_tournament", data: "3" }))
    }, 7000);



    connection.on('close', function(connection) {
        // close user connection
    });
});